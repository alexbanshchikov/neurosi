import struct
import numpy as np


def from_gpr2_to_npy(input_file_name):
    with open(input_file_name, "rb") as in_file:
        byte_array = in_file.read()

    # если нужен i байт, то пишем byte_array[i:i+4]

    tracks_count = int.from_bytes(byte_array[24:28], byteorder="little", signed=True)  # Количество трасс в профиле
    track_length = int.from_bytes(byte_array[28:32], byteorder="little", signed=True)  # Количество точек в тассе
    track_amplitudes = []

    min_i = 1536 + track_length * 4
    max_i = (tracks_count + 1) * (track_length * 4 + 44)
    h = track_length * 4 + 44

    for i in range(min_i, max_i, h):
        min_j = 0
        max_j = track_length * 4
        h_j = 4
        for j in range(min_j, max_j, h_j):
            track_amplitudes.append(struct.unpack('<f', byte_array[i + 44 + j: i + j + 48]))
    track_amplitudes = np.array(track_amplitudes)
    all_amplitudes = np.reshape(track_amplitudes, ( tracks_count, track_length ))
    return all_amplitudes


def from_npy_to_gpr2(input_file_name):
    with open(input_file_name, "rb") as in_file:
        byte_array = in_file.read()

    tracks_count = int.from_bytes(byte_array[24:28], byteorder="little", signed=True)
    track_length = int.from_bytes(byte_array[28:32], byteorder="little", signed=True)
    track_amplitudes = []

    min_i = 1536 + track_length * 4
    max_i = (tracks_count + 1) * (track_length * 4 + 44)
    h = track_length * 4 + 44

    for i in range(min_i, max_i, h):
        min_j = 0
        max_j = track_length * 4
        h_j = 4
        for j in range(min_j, max_j, h_j):
            track_amplitudes.append(struct.unpack('<f', byte_array[i + 44 + j: i + j + 48]))
    track_amplitudes = np.array(track_amplitudes)
    all_amplitudes = np.reshape(track_amplitudes, (tracks_count, track_length))

    # перезаписывание данных
    some_data = bytearray(byte_array[:min_i])
    
    # ----------------------------------------------------
    # process data here
    # example:
    # af.air_alignment_filter(all_amplitudes)
    # ----------------------------------------------------

    for i in range(len(all_amplitudes)):
        some_data += bytearray(byte_array[min_i + h * i: min_i + h * i + 44])  # Записываем хедер трассы
        some_data += bytearray(struct.pack('<%sf' % len(all_amplitudes[i]), *all_amplitudes[i]))  # Записываем трассу
    some_data += bytearray(byte_array[len(some_data):])

    output_file_name = 'out_data.gpr2'
    with open(output_file_name, 'wb') as out_file:
        out_file.write(some_data)


if __name__ == "__main__":

    from_npy_to_gpr2('data.gpr2')
